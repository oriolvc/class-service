# Class service API

A spring boot service for creating and booking classes.

## Endpoints offered:

The present version implements the following:

- `/classes`: (POST) for creating a new class
 ```console
{
  "name": "string",
  "startDate": "2022-03-31",
  "endDate": "2022-03-31",
  "time": "string",
  "capacity": 0
}
 ```
- `/classes`: (GET) for retrieving all existing classes

- `/bookings`: (POST) for booking a class
 ```console
{
  "name": "string",
  "classId": 0,
  "date": "2022-03-31"
}
 ```
- `/bookings`: (GET) for retrieving all existing bookings

## How to run it:

### Option 1: use Docker
1. Having docker configured, open CLI and execute step 2 and 3
2. docker pull oriolvc/class-service:latest
3. docker run -p 8080:8080 oriolvc/class-service
4. navigate to http://localhost:8080/swagger-ui/index.html

### Option 2: run with spring boot
1. Having Maven and Java configured, open CLI and execute step 2 and 3
2. navigate to class-service folder
3. mvn spring-boot:run
4. navigate to http://localhost:8080/swagger-ui/index.html
