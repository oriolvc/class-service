package com.glofox.integration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.glofox.controller.ClassController;
import com.glofox.dto.CreateClassDTO;
import com.glofox.model.ActivityClass;
import com.glofox.repository.ActivityClassRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(controllers = ClassController.class)
public class ClassesControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ActivityClassRepository activityClassRepository;

    @DisplayName("Attempt to create a class with null values")
    @Test
    void createClass_whenINullValues_thenReturns400() throws Exception {
        // given
        final var dto = CreateClassDTO.builder().name("Fitness").build();
        // then
        mockMvc.perform(post("/classes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL)
                        .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest());
    }

    @DisplayName("Attempt to create a class with incorrect time")
    @Test
    void createClass_whenIncorrectTime_thenReturns400() throws Exception {
        // given
        final var dto = CreateClassDTO.builder().name("Fitness").time("009:00")
                .capacity((short) 20).startDate(LocalDate.now()).endDate(LocalDate.now()).build();
        // then
        mockMvc.perform(post("/classes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL)
                        .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest());
    }

    @DisplayName("Attempt to create a class successfully")
    @Test
    void createClass_successfully() throws Exception {
        // given
        final var dto = CreateClassDTO.builder().name("Fitness").time("09:00")
                .capacity((short) 20).startDate(LocalDate.now()).endDate(LocalDate.now()).build();
        final var activityClassEntity = ActivityClass.builder().name("Fitness")
                .time(LocalTime.parse("09:00")).capacity((short) 20).startDate(LocalDate.now()).endDate(LocalDate.now())
                .build();
        // when
        when(activityClassRepository.save(any())).thenReturn(activityClassEntity);
        // then
        mockMvc.perform(post("/classes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL)
                        .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().is2xxSuccessful());
    }

}
