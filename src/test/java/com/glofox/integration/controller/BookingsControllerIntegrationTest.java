package com.glofox.integration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.glofox.controller.BookingController;
import com.glofox.dto.CreateBookingDTO;
import com.glofox.model.Booking;
import com.glofox.model.ActivityClass;
import com.glofox.repository.BookingRepository;
import com.glofox.service.BookingService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(controllers = BookingController.class)
public class BookingsControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BookingService bookingService;

    @MockBean
    private BookingRepository bookingRepository;

    @DisplayName("Attempt to create a booking with null values")
    @Test
    void createBooking_whenNullValue_thenReturns400() throws Exception {
        // given
        final var dto = CreateBookingDTO.builder().name("John Doe").build();
        // then
        mockMvc.perform(post("/bookings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL)
                        .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest());
    }

    @DisplayName("Attempt to create a successful booking")
    @Test
    void createBooking_successfully() throws Exception {
        // given
        final var requestDto = CreateBookingDTO.builder().name("John Doe")
                .classId(1L).date(LocalDate.now()).build();
        final var bookingEntity = Booking.builder().name("John Doe")
                .activityClass(ActivityClass.builder().id(1L).name("Fitness").time(LocalTime.of(9,15)).build())
                .date(LocalDate.now()).build();
        // when
        when(bookingService.bookClass(requestDto.getClassId(), requestDto.getDate(),
                requestDto.getName())).thenReturn(bookingEntity);
        // then
        mockMvc.perform(post("/bookings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.ALL)
                        .content(objectMapper.writeValueAsString(requestDto)))
                .andExpect(status().is2xxSuccessful());
    }
}
