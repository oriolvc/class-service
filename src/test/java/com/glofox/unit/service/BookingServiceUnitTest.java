package com.glofox.unit.service;

import com.glofox.model.ActivityClass;
import com.glofox.model.Booking;
import com.glofox.repository.ActivityClassRepository;
import com.glofox.repository.BookingRepository;
import com.glofox.service.BookingService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BookingServiceUnitTest {

    private BookingService bookingService;

    @Mock
    private BookingRepository bookingRepository;
    @Mock
    private ActivityClassRepository activityClassRepository;

    @BeforeEach
    void setup() {
        bookingService = new BookingService(bookingRepository, activityClassRepository);
    }

    @DisplayName("Create booking with dates out of range")
    @Test
    void createBooking_whenIncorrectDates_throwsException() throws Exception {
        // given
        final var activityClassEntity = ActivityClass.builder().id(1L).name("Fitness")
                .time(LocalTime.parse("09:00")).capacity((short) 20).startDate(LocalDate.now()).endDate(LocalDate.now())
                .build();
        // when
        when(activityClassRepository.findById(1L)).thenReturn(Optional.of(activityClassEntity));
        // then
        assertThrows(IllegalArgumentException.class, () -> bookingService
                .bookClass(1L, LocalDate.now().plusDays(3), "John Doe"));

    }

    @DisplayName("Create booking for non-existing class id")
    @Test
    void createBooking_whenNonExistingClass_throwsException() throws Exception {
        // given
        final var activityClassEntity = ActivityClass.builder().id(1L).name("Fitness")
                .time(LocalTime.parse("09:00")).capacity((short) 20).startDate(LocalDate.now()).endDate(LocalDate.now())
                .build();
        // when
        when(activityClassRepository.findById(1L)).thenReturn(Optional.empty());
        // then
        assertThrows(IllegalArgumentException.class, () -> bookingService
                .bookClass(1L, LocalDate.now().plusDays(3), "John Doe"));

    }

    @DisplayName("Create booking successfully")
    @Test
    void createBooking_successfully() throws Exception {
        // given
        final var activityClassEntity = ActivityClass.builder().id(1L).name("Fitness")
                .time(LocalTime.parse("09:00")).capacity((short) 20).startDate(LocalDate.now()).endDate(LocalDate.now())
                .build();
        final var expectedBooking = Booking.builder().name("John Doe").date(LocalDate.now())
                .activityClass(activityClassEntity).build();
        // when
        when(activityClassRepository.findById(1L)).thenReturn(Optional.of(activityClassEntity));
        when(bookingRepository.save(any())).thenReturn(expectedBooking);
        final var returnedBooking = bookingService.bookClass(1L, LocalDate.now(), "John Doe");
        // then
        assertThat(expectedBooking).isEqualTo(returnedBooking);

    }
}
