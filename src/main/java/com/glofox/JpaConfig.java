package com.glofox;

import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.glofox.repository")
@EnableJpaAuditing
public class JpaConfig {
}
