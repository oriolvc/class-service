package com.glofox.dto.converter;

import com.glofox.dto.ViewClassDTO;
import com.glofox.dto.CreateClassDTO;
import com.glofox.model.ActivityClass;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class ClassConverter {

    public static ActivityClass toEntity(CreateClassDTO createSessionDTO){
        return ActivityClass.builder()
                .name(createSessionDTO.getName())
                .startDate(createSessionDTO.getStartDate())
                .endDate(createSessionDTO.getEndDate())
                .time(LocalTime.parse(createSessionDTO.getTime()))
                .capacity(createSessionDTO.getCapacity())
                .build();
    }

    public static ViewClassDTO toDTO(ActivityClass sessionEntity){
        return ViewClassDTO.builder()
                .id(sessionEntity.getId())
                .name(sessionEntity.getName())
                .startDate(sessionEntity.getStartDate())
                .endDate(sessionEntity.getEndDate())
                .time(sessionEntity.getTime().format(DateTimeFormatter.ofPattern("hh:mm")))
                .capacity(sessionEntity.getCapacity())
                .build();
    }

    public static List<ViewClassDTO> toDTO(List<ActivityClass> sessionEntityList){
        return sessionEntityList.stream().map(e -> toDTO(e)).collect(Collectors.toList());
    }
}
