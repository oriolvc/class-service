package com.glofox.dto.converter;

import com.glofox.dto.ViewBookingDTO;
import com.glofox.model.Booking;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class BookingConverter {

    public static ViewBookingDTO toDTO(Booking booking){
        return ViewBookingDTO.builder()
                .id(booking.getId())
                .name(booking.getName())
                .className(booking.getActivityClass().getName())
                .date(booking.getDate())
                .time(booking.getActivityClass().getTime().format(DateTimeFormatter.ofPattern("hh:mm")))
                .build();
    }

    public static List<ViewBookingDTO> toDTO(List<Booking> bookingEntityList){
        return bookingEntityList.stream().map(e -> toDTO(e)).collect(Collectors.toList());
    }
}
