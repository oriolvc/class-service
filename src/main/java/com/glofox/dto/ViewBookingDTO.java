package com.glofox.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Builder
public class ViewBookingDTO {
    private Long id;
    private String name;
    private String className;
    private LocalDate date;
    private String time;
}
