package com.glofox.controller;

import com.glofox.dto.ViewBookingDTO;
import com.glofox.dto.converter.BookingConverter;
import com.glofox.dto.CreateBookingDTO;
import com.glofox.repository.BookingRepository;
import com.glofox.service.BookingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Bookings")
@RestController
@RequiredArgsConstructor
@RequestMapping("/bookings")
public class BookingController {

    private final BookingService bookingService;
    private final BookingRepository bookingRepository;

    @Operation(summary = "Find bookings",
            description = "Returns all existing bookings")
    @GetMapping()
    public ResponseEntity<List<ViewBookingDTO>> getBookings() throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(BookingConverter.toDTO(bookingRepository.findAll()));
    }

    @Operation(summary = "Create booking",
            description = "Creates a booking for given class. Session id must match a existing class. " +
                    "Date format must be yyyy-MM-dd. Eg: 2022-03-30")
    @PostMapping()
    public ResponseEntity<ViewBookingDTO> createClass(@Valid @RequestBody CreateBookingDTO dto) throws Exception {
        return ResponseEntity.status(HttpStatus.OK)
                .body(BookingConverter.toDTO(bookingService.bookClass(dto.getClassId(), dto.getDate(), dto.getName())));
    }
}
