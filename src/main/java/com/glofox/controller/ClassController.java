package com.glofox.controller;

import com.glofox.dto.ViewClassDTO;
import com.glofox.dto.CreateClassDTO;
import com.glofox.dto.converter.ClassConverter;
import com.glofox.repository.ActivityClassRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Classes")
@RestController
@RequiredArgsConstructor
@RequestMapping("/classes")
public class ClassController {

    private final ActivityClassRepository activityClassRepository;

    @Operation(summary = "Find classes",
            description = "Returns all available classes")
    @GetMapping()
    public ResponseEntity<List<ViewClassDTO>> getClasses() throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(ClassConverter.toDTO(activityClassRepository.findAll()));
    }

    @Operation(summary = "Create class",
            description = "Allows creating a class with basic information. " +
                    "Date format must be yyyy-MM-dd, time pattern must be like hh:mm, Ex: 09:00 ")
    @PostMapping()
    public ResponseEntity<ViewClassDTO> createClass(@Valid @RequestBody CreateClassDTO dto) throws Exception {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ClassConverter.toDTO(activityClassRepository.save(ClassConverter.toEntity(dto))));
    }
}
