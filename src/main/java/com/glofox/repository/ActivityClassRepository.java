package com.glofox.repository;

import com.glofox.model.ActivityClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityClassRepository extends JpaRepository<ActivityClass, Long> {
}
