package com.glofox.service;

import com.glofox.model.Booking;
import com.glofox.model.ActivityClass;
import com.glofox.repository.BookingRepository;
import com.glofox.repository.ActivityClassRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class BookingService {

    private final BookingRepository bookingRepository;
    private final ActivityClassRepository activityClassRepository;

    public Booking bookClass(Long classId, LocalDate requestedDate, String memberName) throws Exception {
        final var activityClass = activityClassRepository.findById(classId)
                .orElseThrow(() -> new IllegalArgumentException("No class found with given id"));
        if (!isRequestedDateInSessionRange(requestedDate, activityClass)){
            throw new IllegalArgumentException("Requested date is not within class dates");
        }
        return bookingRepository.save(Booking.builder()
                .name(memberName)
                .date(requestedDate)
                .activityClass(activityClass)
                .build());
    }

    private Boolean isRequestedDateInSessionRange(LocalDate requestedDate, ActivityClass session){
        if ((requestedDate.isEqual(session.getStartDate()) || requestedDate.isAfter(session.getStartDate()))
            && (requestedDate.isEqual(session.getEndDate()) || requestedDate.isBefore(session.getEndDate()))){
            return true;
        }
        return false;
    }
}
