package com.glofox.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private LocalDate date;

    @ManyToOne(fetch = FetchType.LAZY)
    private ActivityClass activityClass;

    @CreatedDate
    private LocalDate createDate;

    @LastModifiedDate
    private LocalDate lastModifiedDate;
}
